;;; schelp-mode.el --- An Emacs major mode for editing SuperCollider Help files

;; Copyright (C) 2014  Joachim Mathes
;;
;; Author: Joachim Mathes <joachim <underscore> mathes <at> web <dot> de>
;; Created: October 2014
;; Version: 0.0.1
;; Keywords: files

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Commentary:

;; This is a major mode for editing SuperCollider Help files.  It is developed to
;; support syntax highlighting.
;; See http://doc.sccode.org/Reference/SCDocSyntax.html

;; Installation:

;; To install just drop this file into a directory on your load-path and
;; byte-compile it.  To set up Emacs to automatically edit files ending in ".schelp"
;; using schelp-mode add the following to your ~/.emacs file (GNU Emacs) or
;; ~/.xemacs/init.el file (XEmacs):
;;    (setq auto-mode-alist (cons '("\\.schelp$" . schelp-mode) auto-mode-alist))
;;    (autoload 'schelp-mode "schelp-mode" "SuperCollider Help file editing mode." t)

;; This file is *NOT* part of GNU Emacs.

;;; Code:
(defconst schelp-version "0.0.1"
  "`schelp-mode' version number.")

;;;; User definable variables

(defgroup schelp nil
  "Major mode for editing SuperCollider Help files."
  :prefix "schelp-"
  :group 'languages)

(defcustom schelp-block-indentation 2
  "The indentation relative to a predecessing line which begins a new block."
  :type 'integer
  :group 'schelp)

(defcustom schelp-newline-function 'newline-and-indent
  "Function to be called upon pressing `RET'."
  :type '(choice (const newline)
		 (const newline-and-indent)
		 (const reindent-then-newline-and-indent))
  :group 'schelp)

(defcustom schelp-header-tag-color "DarkTurquoise"
  "The foreground color used to highlight headers."
  :type 'color
  :group 'schelp)

(defcustom schelp-section-tag-color "DodgerBlue1"
  "The foreground color used to highlight sections."
  :type 'color
  :group 'schelp)

(defcustom schelp-method-tag-color "maroon"
  "The foreground color used to highlight methods."
  :type 'color
  :group 'schelp)

(defcustom schelp-modal-tag-color "ForestGreen"
  "The foreground color used to highlight methods."
  :type 'color
  :group 'schelp)

(defcustom schelp-list-tag-color "DodgerBlue1"
  "The foreground color used to highlight lists."
  :type 'color
  :group 'schelp)

(defcustom schelp-note-tag-color "DarkRed"
  "The foreground color used to highlight notes."
  :type 'color
  :group 'schelp)

(defcustom schelp-other-tag-color "SaddleBrown"
  "The foreground color used to highlight other tags."
  :type 'color
  :group 'schelp)

;;;; Internal variables

(defvar schelp-mode-hook nil
  "Hook called by `schelp-mode'.")

(defvar schelp-header-tag-face 'schelp-header-tag-face
  "Face for header tags.")

(defvar schelp-section-tag-face 'schelp-section-tag-face
  "Face for section tags.")

(defvar schelp-method-tag-face 'schelp-method-tag-face
  "Face for method tags.")

(defvar schelp-modal-tag-face 'schelp-modal-tag-face
  "Face for modal tags.")

(defvar schelp-list-tag-face 'schelp-list-tag-face
  "Face for list tags.")

(defvar schelp-note-tag-face 'schelp-note-tag-face
  "Face for note tags.")

(defvar schelp-bold-face 'schelp-bold-face
  "Face for bold text.")

(defvar schelp-italic-face 'schelp-italic-face
  "Face for italic text.")


(defun schelp-font-lock-mode-hook ()
  "Defines a SuperCollider Help font lock mode hook."
  (copy-face 'font-lock-builtin-face 'schelp-header-tag-face)
  (set-face-foreground 'schelp-header-tag-face
		       schelp-header-tag-color)

  (copy-face 'font-lock-builtin-face 'schelp-section-tag-face)
  (set-face-foreground 'schelp-section-tag-face
		       schelp-section-tag-color)

  (copy-face 'font-lock-builtin-face 'schelp-method-tag-face)
  (set-face-foreground 'schelp-method-tag-face
		       schelp-method-tag-color)

  (copy-face 'font-lock-builtin-face 'schelp-modal-tag-face)
  (set-face-foreground 'schelp-modal-tag-face
		       schelp-modal-tag-color)

  (copy-face 'font-lock-builtin-face 'schelp-list-tag-face)
  (set-face-foreground 'schelp-list-tag-face
		       schelp-list-tag-color)

  (copy-face 'font-lock-builtin-face 'schelp-note-tag-face)
  (set-face-foreground 'schelp-note-tag-face
		       schelp-note-tag-color)

  (copy-face 'font-lock-builtin-face 'schelp-other-tag-face)
  (set-face-foreground 'schelp-other-tag-face
		       schelp-other-tag-color)

  (copy-face 'font-lock-builtin-face 'schelp-bold-face)
  (set-face-foreground 'schelp-bold-face
  		       (frame-parameter nil 'foreground-color))
  (set-face-attribute 'schelp-bold-face nil
		      :weight 'ultra-bold)

  (copy-face 'font-lock-builtin-face 'schelp-italic-face)
  (set-face-foreground 'schelp-italic-face
  		       (frame-parameter nil 'foreground-color))
  (set-face-attribute 'schelp-italic-face nil
		      :slant 'italic))

(defvar schelp-tags
  '((header "TITLE" "CLASS" "CATEGORIES" "RELATED" "SUMMARY" "REDIRECT")
    (section "SECTION" "DESCRIPTION" "CLASSMETHODS" "INSTANCEMETHODS" "EXAMPLES" "SUBSECTION")
    (method "METHOD" "PRIVATE" "COPYMETHOD" "ARGUMENT" "RETURNS" "DISCUSSION")
    (modal "STRONG" "EMPHASIS" "SOFT" "LINK" "ANCHOR" "IMAGE" "CODE" "TELETYPE")
    (list "TABLE" "DEFINITIONLIST" "LIST" "NUMBEREDLIST" "TREE")
    (note "NOTE" "WARNING" "FOOTNOTE")
    (other "KEYWORD" "CLASSTREE"))
  "SCHelp tags.")

(defvar schelp-font-lock-keywords
  (let ((ored-header-tags (mapconcat 'identity (cdr (assoc 'header schelp-tags)) "\\|"))
	(ored-section-tags (mapconcat 'identity (cdr (assoc 'section schelp-tags)) "\\|"))
	(ored-method-tags (mapconcat 'identity (cdr (assoc 'method schelp-tags)) "\\|"))
	(ored-modal-tags (mapconcat 'identity (cdr (assoc 'modal schelp-tags)) "\\|"))
	(ored-list-tags (mapconcat 'identity (cdr (assoc 'list schelp-tags)) "\\|"))
	(ored-note-tags (mapconcat 'identity (cdr (assoc 'note schelp-tags)) "\\|"))
	(ored-other-tags (mapconcat 'identity (cdr (assoc 'other schelp-tags)) "\\|")))
  (list
   (list (concat "\\(" ored-header-tags "\\)::") 1 'schelp-header-tag-face t)
   (list (concat "\\(" ored-section-tags "\\)::") 1 'schelp-section-tag-face t)
   (list (concat "\\(" ored-method-tags "\\)::") 1 'schelp-method-tag-face t)
   (list (concat "\\(" ored-modal-tags "\\)::") 1 'schelp-modal-tag-face t)
   (list (concat "\\(" ored-list-tags "\\)::") 1 'schelp-list-tag-face t)
   (list (concat "\\(" ored-note-tags "\\)::") 1 'schelp-note-tag-face t)
   (list (concat "\\(" ored-other-tags "\\)::") 1 'schelp-other-tag-face t)
   (list "^[ \t]*\\(##\\|||\\)" 1 'schelp-list-tag-face t)
   '("^[ \t]*##" "\\(||\\)" nil nil (0 'schelp-list-tag-face))
   (list "STRONG::\\(.*\\)::" 1 'schelp-bold-face t)
   (list "EMPHASIS::\\(.*\\)::" 1 'schelp-italic-face t)))
"Expressions to highlight in SCHelp mode.")

(defvar schelp-mode-syntax-table nil
  "Syntax table used in SCHelp Mode buffers.")

(defvar schelp-mode-map ()
  "Key map used in SCHelp Mode buffers.")

;;;; Functions

;;;###autoload
(defun schelp-mode ()
  "Major mode for editing SuperCollider Help (SCHelp) files.
Bug reports, suggestions for new features and critics should go to
`joachim_mathes@web.de'.

This mode knows about syntax highlighting.

COMMANDS
\\{schelp-mode-map}
VARIABLES
"
  (interactive)
  (kill-all-local-variables)
  (make-local-variable 'font-lock-defaults)
  (make-local-variable 'indent-line-function)
  (make-local-variable 'tab-always-indent)

  (when (not schelp-mode-syntax-table)
    (setq schelp-mode-syntax-table (make-syntax-table)))
  (set-syntax-table schelp-mode-syntax-table)

  (add-hook 'font-lock-mode-hook 'schelp-font-lock-mode-hook)

  (if schelp-mode-map
      nil
    (setq schelp-mode-map (make-sparse-keymap))
    (define-key schelp-mode-map "\r" 'schelp-newline)
    (define-key schelp-mode-map "\C-c\C-m" 'schelp-insert-tag))
  (use-local-map schelp-mode-map)

  (setq major-mode           	'schelp-mode
	mode-name	        "SCHelp"
	font-lock-defaults	'(schelp-font-lock-keywords nil t)
	indent-line-function 	'schelp-indent-line
	completion-ignore-case 	t
	tab-always-indent    	t)

  ;; Run the mode hook.
  (if schelp-mode-hook
      (run-hooks 'schelp-mode-hook)))

(defun schelp-newline ()
  "Call the dedicated newline function.

The variable `schelp-newline-function' decides which newline function to
use."
  (interactive)
  (funcall schelp-newline-function))

(defun schelp-insert-tag ()
  "Insert tag"
  (interactive)
  (let ((tag (completing-read "Tag: " (schelp-tags-by-category) nil t)))
    (cond
     ((member tag (schelp-tags-by-category 'header))
      (insert (concat tag "::")))
     ((member tag (schelp-tags-by-category 'modal))
      (insert (concat tag "::::"))
      (backward-char 2))
     ((member tag (schelp-tags-by-category 'list))
      (insert (concat tag "::\n\n::"))
      (schelp-indent-line)
      (forward-line -1)
      (beginning-of-line)
      (insert "## ")
      (schelp-indent-line))
     (t
      (insert (concat tag "::"))))))

(defun schelp-tags-by-category (&optional category)
  "Return tags of CATEGORY
Return all tags if CATEGORY is nil."
  (if (not category)
      (let (value)
	(dolist (tags schelp-tags value)
	  (setq value (append value (cdr tags)))))
    (cdr (assoc category schelp-tags))))

(defun schelp-indent-line ()
  "Indent current line for SCHelp mode."
  (let ((current-point-backup (point))
	(current-column-backup (current-column))
	(current-indentation-backup (current-indentation))
	(calculated-indent-column 0))

    (save-excursion
      (beginning-of-line)
      (cond
       ((looking-at "[ \t]*##")
	(re-search-backward (concat "\\(" (mapconcat 'identity (cdr (assoc 'list schelp-tags)) "\\|") "\\)::\\|^[ \t]*\\(::\\)[ \t]*$"))
	(cond
	 ((match-string 1)
	  (setq calculated-indent-column (+ (current-column) schelp-block-indentation)))
	 ((match-string 2)
	  (back-to-indentation)
	  (setq calculated-indent-column (current-column)))))
       ((looking-at "[ \t]*::")
	(let ((open-blocks 1)
	      (tags (append (schelp-tags-by-category 'list) '("CODE" "TELETYPE"))))
	  (while (> open-blocks 0)
	    (re-search-backward (concat "\\(" (mapconcat 'identity tags "\\|") "\\)::\\|^[ \t]*\\(::\\)[ \t]*$"))
	    (cond
	     ((match-string 1)
	      (setq open-blocks (1- open-blocks))
	      (setq calculated-indent-column (current-column)))
	     ((match-string 2)
	      (setq open-blocks (1+ open-blocks))
	      (back-to-indentation)
	      (setq calculated-indent-column (current-column)))
	     (t
	      (setq open-blocks 0))))))
       (t
       	(forward-line -1)
       	(while (and (looking-at "^[ \t]*\n")
       		    (not (bobp)))
       	  (forward-line -1))
       	(back-to-indentation)
	(setq calculated-indent-column (current-column))
	)))

    ;; Set indentation value on current line
    (back-to-indentation)
    (backward-delete-char-untabify (current-column))
    (indent-to calculated-indent-column)
    (if (> current-column-backup current-indentation-backup)
	(forward-char (- current-column-backup current-indentation-backup)))))

(provide 'schelp-mode)

;;; schelp-mode.el ends here
