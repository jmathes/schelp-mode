# SuperCollider Help Mode for Emacs #

This is an Emacs Mode for SCHelp files.

It supports

* Syntax highlighting of [SCHelp keywords](http://doc.sccode.org/Reference/SCDocSyntax.html)
* Keyword proposals on `C-c C-m`
* Indentation of lists

Feel free to adjust it to your needs.
